import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(values: any, selectedValue:any): any[] {

    return values.filter(option=>option!==selectedValue);

  }

}
