import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TempDropComponent } from './temp-drop.component';

describe('TempDropComponent', () => {
  let component: TempDropComponent;
  let fixture: ComponentFixture<TempDropComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TempDropComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TempDropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
