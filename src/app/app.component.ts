import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedService } from './shared.service';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'New App';

  allTeams: any;
  LoginStatusFetched: Boolean = true;

  constructor(private http: HttpClient, private userService: UserService) {}

  ngOnInit() {
    this.LoginStatusFetched = this.userService.userLoginStatus;

  }
}
