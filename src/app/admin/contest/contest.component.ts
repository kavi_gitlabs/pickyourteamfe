import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ContestService } from 'src/app/contest.service';
import { MapserviceService } from 'src/app/mapservice.service';

@Component({
  selector: 'app-contest',
  templateUrl: './contest.component.html',
  styleUrls: ['./contest.component.scss'],
})
export class ContestComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private contestService: ContestService,
    public mapContestService: MapserviceService
  ) {}

  // Variable Declares
  contestform: FormGroup;
  contests: any;
  editStatus: boolean = true;
  id: string;

  // Form Control variables
  contestname = new FormControl('', [Validators.required]);
  entryfee = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);
  maxsize = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);
  minsize = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);
  prizemoney = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);
  rank1 = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);
  rank2 = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);
  rank3 = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);

  ngOnInit(): void {
    
    this.contestform = this.fb.group({
      contestname: this.contestname,
      maxsize: this.maxsize,
      minsize: this.minsize,
      prizemoney: this.prizemoney,
      entryfee: this.entryfee, 
      rank1: this.rank1,
      rank2: this.rank2,
      rank3: this.rank3
    });
    this.viewContests();
  } 
  
  viewContests() {
    this.contestService.getAllContests().subscribe({
      next: (res) => {
        console.log(res);
        this.contests = res;
        this.mapContestService.finalCurrentContests = res;
      },
    });
  }

  onFormSubmit() {
    console.log(this.contestform.value.date);
  }
  createContest() {
    this.router.navigateByUrl('admin/createContest');
  }
  edit(user: any) {
    this.editStatus = false;
    this.id = user.id;
    console.log(user);

    this.contestform.setValue({

      contestname: user.contestname,
      maxsize: user.maxsize,
      minsize: user.minsize,
      prizemoney: user.prizemoney,
      entryfee: user.entryfee,
      rank1: user.rank1,
      rank2: user.rank2,
      rank3: user.rank3
      

    });
  }

  save() {
    let modifiedContest = this.contestform.value;
    modifiedContest.id = this.id;

    this.contestService.editContest(modifiedContest).subscribe({
      next: (res) => {
        alert(`Contest Modified Successfully`);
        this.editStatus = true;
        this.viewContests();
      },
    }); 
  }

  cancel() {
    this.editStatus = true;
  }
  delete(user: any) {
    this.id = user.id;
    console.log(user);
    if (confirm(`Are you sure you want to delete the Match between  `)) {
      this.contestService.deleteContest(this.id).subscribe({
        next: (res) => {
          alert(`Match data of ---deleted Successfully`);
          this.viewContests();
        },
      });
    }
  }
}
