import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsermatchesComponent } from './usermatches.component';

describe('UsermatchesComponent', () => {
  let component: UsermatchesComponent;
  let fixture: ComponentFixture<UsermatchesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsermatchesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UsermatchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
