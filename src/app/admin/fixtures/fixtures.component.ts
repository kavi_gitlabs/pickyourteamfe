import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup, 
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { FixtureService } from 'src/app/fixture.service';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-fixtures',
  templateUrl: './fixtures.component.html',
  styleUrls: ['./fixtures.component.scss'],
})
export class FixturesComponent implements OnInit {
  constructor(
    private fixService: FixtureService,
    private fb: FormBuilder,
    private router: Router,
    private teamService: SharedService
  ) {}
  fixtureform: FormGroup;
  editStatus: boolean = true;
  id: string;
  fixtures: any;
  teams: any;
  editProductIndex: number;
  
  teamsraw = [];
  selectedTeam1 = [];
  selectedTeam2 = [];



  team1 = new FormControl('', [Validators.required, Validators.minLength(4)]);
  team2 = new FormControl('', [Validators.required, Validators.minLength(4)]);

  datetime = new FormControl(null, [
    Validators.required,
    Validators.minLength(5),
  ]);

  venue = new FormControl('', [Validators.required]);
  stage = new FormControl('', [Validators.required]);







  
  ngOnInit(): void {
    this.viewFixtures();
    this.viewTeams();


    this.selectedTeam1 = this.teamsraw;
    this.selectedTeam2 = this.teamsraw;


    this.fixtureform = this.fb.group({
      team1: this.team1,
      team2: this.team2,
      datetime: this.datetime,
      venue: this.venue,
      stage: this.stage

    });
  }
  cancel() {
    this.editStatus = true;

  } 


  
  clicked1() {
    this.selectedTeam2 = this.teamsraw;
    this.selectedTeam2 = this.selectedTeam2.filter(
      (team) => team !== this.fixtureform.value.team1
    );
   
  }
 
  
  clicked2() {
    this.selectedTeam1 = this.teamsraw;
    this.selectedTeam1 = this.selectedTeam1.filter(
      (team) => team !== this.fixtureform.value.team2
      );
  }


  // All fixture
  viewFixtures() {
    this.fixService.getAllFixtures().subscribe({
      next: (res) => {
        console.log(res);
        this.fixtures = res;
      },
    });
  }

  viewTeams() {
    this.teamService.getAllTeams().subscribe({
      next: (res) => {
        console.log(res);
        this.teams = res;
        this.teams.forEach((val) => this.teamsraw.push(val.name));

      },
    });
  }

  onFormSubmit() {
    console.log(this.fixtureform.value.date);
  }
  createFixture() {
    this.router.navigateByUrl('admin/newfixtures');
  }
  edit(user) {
    this.editStatus = false;
    this.id = user.id;
    this.fixtureform.setValue({
      team1: user.team1,
      team2: user.team2,
      venue: user.venue,
      datetime: user.datetime,
      stage: user.stage
    });
    console.log(user);
  }

  save() {
    let modifiedFixture = this.fixtureform.value;
    modifiedFixture.id = this.id;

    this.fixService.editFixture(modifiedFixture).subscribe({
      next: (res) => {
        alert(
          `Match Modified Successfully`
        );
        this.editStatus = true;
        this.viewFixtures();
      },
    });
  }
  delete(user: any) {
    this.id = user.id;
    console.log(user);
    if (confirm(`Are you sure you want to delete the Match between ---${user.team1}--- and ---${user.team2}?--- `)) {
      this.fixService.deleteFixture(this.id).subscribe({
        next: (res) => {
          alert(`Match data of ---${user.team1}--- and ---${user.team2}--- deleted Successfully`);
          this.viewFixtures();
        },
      });
    }
  }
}
