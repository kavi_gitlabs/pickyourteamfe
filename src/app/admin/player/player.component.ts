import { Component, OnInit, PlatformRef } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup, 
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { PlayerService } from 'src/app/player.service';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent implements OnInit {
  constructor(
    private teamService: SharedService,
    private fb: FormBuilder, 
    private playerService: PlayerService,
    private router: Router
  ) {}
  allplayers: any;
  teams: any;
  editStatus: boolean = true;
  editProductIndex: number;
  id: string;
  editTable: FormGroup;

  name = new FormControl('', [Validators.required, Validators.minLength(4)]);

  points = new FormControl(0, [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]); 
  team_name = new FormControl('', [
    Validators.required,
    Validators.minLength(5),
  ]);
  skills = new FormControl('', [Validators.required]);

 
  ngOnInit(): void {
    this.viewPlayers();
    this.viewTeams();

    this.editTable = this.fb.group({
      name: this.name,
      points: this.points,
      team_name: this.team_name,
      skills: this.skills,
    });
  }

  viewPlayers() {
    this.playerService.getAllPlayers().subscribe({
      next: (res) => {
        this.allplayers = res;
        console.log('LODFSAGDAS', this.allplayers.length, 'LODFSAGDAS');
      },
    });
  }
  viewTeams() {
    this.teamService.getAllTeams().subscribe({
      next: (res) => {
        console.log(res);
        this.teams = res;
      }, 
    });
  }

  createPlayer() { 
    this.router.navigateByUrl('admin/newplayer');
  }

  edit(user: any) {
    this.editStatus = false;
    this.id = user.id;
    this.editTable.setValue({
      name: user.name,
      team_name: user.team_name,
      points: user.points,
      skills: user.skills,
    });
  }
  cancel() {
    this.editStatus = true;
  }

  save() {
    let modifiedUser = this.editTable.value;
    modifiedUser.id = this.id;

    this.playerService.editPlayer(modifiedUser).subscribe({
      next: (res) => {
        alert(`User data of --${modifiedUser.name}-- edited Successfully`);
        this.editStatus = true;
        this.viewPlayers();
      },
    });
  }

  delete(user: any) {
    this.id = user.id;
    if (
      confirm(`Are you sure you want to delete Player Data : ${user.name} ?`)
    ) {
      this.playerService.deletePlayer(this.id).subscribe({
        next: (res) => {
          alert(`Player data of ---- deleeted Successfully`);
          this.viewPlayers();
        },
      });
    }
  }
}
