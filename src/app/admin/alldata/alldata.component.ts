import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';

@Component({ 
  selector: 'app-alldata',
  templateUrl: './alldata.component.html',
  styleUrls: ['./alldata.component.scss']
})
export class AlldataComponent implements OnInit {

  constructor(    private userService: UserService
    ) { }

  ngOnInit(): void {
    this.viewUsers();
  }
  teams: any;
  teamsLength: any;


  viewUsers() {
    this.userService.getAllUsers().subscribe({
      next: (res) => {
        // console.log(res)
        this.teams = res;
        this.teamsLength = this.teams.length;
      },
    });
  }

}
