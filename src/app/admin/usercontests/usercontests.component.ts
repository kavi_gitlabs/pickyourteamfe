import { Component, OnInit } from "@angular/core";
import { ContestService } from "src/app/contest.service";
import { MapserviceService } from "src/app/mapservice.service";
import { UserService } from "src/app/user.service";

@Component({
  selector: "app-usercontests",
  templateUrl: "./usercontests.component.html", 
  styleUrls: ["./usercontests.component.scss"],
})
export class UsercontestsComponent implements OnInit {
  constructor(
    public mapContestService: MapserviceService,
    public userService: UserService
  ) {}

  ngOnInit(): void {
  }

}
 