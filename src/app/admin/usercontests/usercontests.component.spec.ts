import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsercontestsComponent } from './usercontests.component';

describe('UsercontestsComponent', () => {
  let component: UsercontestsComponent;
  let fixture: ComponentFixture<UsercontestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsercontestsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UsercontestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
