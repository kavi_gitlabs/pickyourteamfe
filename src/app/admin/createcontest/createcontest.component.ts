import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ContestService } from 'src/app/contest.service';

@Component({
  selector: 'app-createcontest',
  templateUrl: './createcontest.component.html',
  styleUrls: ['./createcontest.component.scss'],
})
export class CreatecontestComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private contestService: ContestService
  ) {}
  ngOnInit(): void {
    this.contestform = this.fb.group({
      contestname: this.contestname,
      maxsize: this.maxsize,
      minsize: this.minsize,
      prizemoney: this.prizemoney,
      entryfee: this.entryfee, 
      rank1: this.rank1,
      rank2: this.rank2,
      rank3: this.rank3,
    });
  }

  // DEclare Variables
  contestform: FormGroup;
  editStatus: boolean = true;
  id: string;

  // Form Control variables
  contestname = new FormControl('', [Validators.required]);
  entryfee = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);
  maxsize = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);
  minsize = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);
  prizemoney = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);
  rank1 = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);
  rank2 = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);
  rank3 = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]*$'),
  ]);

  save() {
    let modifiedContest = this.contestform.value;
    modifiedContest.id = this.id;

    this.contestService.createContest(modifiedContest).subscribe({
      next: (res) => {
        alert(`Contest created Successfully`);
        this.router.navigateByUrl('/admin/contest');
        // this.viewContests();
      },
    });
  }
  cancel() {
    this.router.navigateByUrl('/admin/contest');
  }
}
