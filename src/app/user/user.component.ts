import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FixtureService } from "../fixture.service";
import { MapserviceService } from "../mapservice.service";
import { SharedService } from "../shared.service";
import { UserService } from "../user.service";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"],
})
export class UserComponent implements OnInit {
  finalCurrentUser: any;
  constructor(
    private userService: UserService,
    private fixService: FixtureService,
    private teamService: SharedService,
    private mapContestService: MapserviceService,
    private router: Router 
    , private route: ActivatedRoute
  ) {}
  allfixtures: any;
  allteams: any;

  loginStatus: boolean = false;

  ngOnInit(): void {
    this.finalCurrentUser = this.userService.currentUser;
    this.viewFixtures();
    this.viewTeams();
    // console.log(this.finalCurrentUser, 'Final User start');

    if (this.finalCurrentUser === undefined) {
      this.loginStatus = true;
    } else {
      this.loginStatus = true;
    }
  }


  viewFixtures() {
    this.fixService.getAllFixtures().subscribe({
      next: (res) => {
        this.allfixtures = res;
        // console.log(this.allfixtures);
      },
    });
  }

  logout() {
    // confirm()
    // this.loginStatus = false;
    // this.finalCurrentUser = undefined;
  }
  login() {
    this.router.navigateByUrl("login");
  }
 
  viewTeams() {
    this.teamService.getAllTeams().subscribe({
      next: (res) => {
        this.allteams = res;
        // console.log(this.allteams);
      },
    });
  }



}
